package com.tora;

import logic.Operations;
import models.DivisionByZero;
import org.junit.Test;

import static java.lang.Math.abs;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.number.OrderingComparison.greaterThan;
import static org.junit.Assert.assertThrows;
import static org.junit.Assert.assertTrue;

public class OperationsTest {
    private final static double epsilon=0.00000001;
    @Test
    public void testForAddValues() {
        double x=Operations.add(2.3,1);
        double expected=3.3;
        assertThat(epsilon, greaterThan(abs(x-expected)));
    }

    @Test
    public void testForDivisionByZero() {
        Exception exception = assertThrows(DivisionByZero.class, () -> Operations.div(-10, 0));
        String expectedMessage = "Division by zero";
        String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(expectedMessage));
    }

    @Test
    public void testForSubValues() {
        double x=Operations.sub(2.3,1);
        double expected=1.3;
        assertThat(epsilon, greaterThan(abs(x-expected)));
    }

    @Test
    public void testForMulValues() {
        double x=Operations.mul(2.3,2);
        double expected=4.6;
        assertThat(epsilon, greaterThan(abs(x-expected)));
    }

    @Test
    public void testForDivValues() throws DivisionByZero {
        double x=Operations.div(2.2,2);
        double expected=1.1;
        assertThat(epsilon, greaterThan(abs(x-expected)));
    }
}
