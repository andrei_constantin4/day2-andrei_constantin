package logic;

import models.DivisionByZero;

public class Operations {
    public static double add(double x, double y){
        return x+y;
    }
    public static double sub(double x, double y){
        return x-y;
    }
    public static double div(double x, double y) throws DivisionByZero {
        if(y==0.0){
            throw new DivisionByZero();
        }
        return x/y;
    }

    public static double mul(double x,double y){
        return x*y;
    }
}
