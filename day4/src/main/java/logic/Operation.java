package logic;

import models.DivisionByZero;

public interface Operation<T> {
    abstract T compute(T x, T y) throws DivisionByZero;
}
