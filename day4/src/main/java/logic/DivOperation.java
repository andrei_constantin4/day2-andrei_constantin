package logic;

import models.DivisionByZero;

public class DivOperation implements Operation<Double>{
    private final static double epsilon=0.00000001;
    @Override
    public Double compute(Double x, Double y) throws DivisionByZero {
        if(y<epsilon){
            throw new DivisionByZero();
        }
        return x/y;
    }
}
