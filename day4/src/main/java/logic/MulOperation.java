package logic;

import models.DivisionByZero;

public class MulOperation implements Operation<Double>{
    @Override
    public Double compute(Double x, Double y) {
        return x*y;
    }
}
