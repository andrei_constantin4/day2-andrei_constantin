package logic;

public class SubOperation implements Operation<Double>{
    @Override
    public Double compute(Double x, Double y) {
        return x-y;
    }
}
