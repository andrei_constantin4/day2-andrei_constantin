package logic;

public class AddOperation implements Operation<Double>{
    @Override
    public Double compute(Double x, Double y) {
        return x+y;
    }
}
