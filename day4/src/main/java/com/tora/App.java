package com.tora;

import logic.AddOperation;
import logic.Controller;
import logic.DivOperation;
import logic.Operation;
import models.DivisionByZero;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class App {
    public static void main(String[] args) throws DivisionByZero {
        /*try {
            Controller.loop();
        } catch (IOException e) {
            System.err.println("IOException");
        }*/

        List<Operation<Double>> list=new ArrayList<>();
        list.add(new AddOperation());
        list.add(new DivOperation());
        for(Operation<Double>x:list){
            x.compute(1.1,1.0);
            if(x instanceof AddOperation){
                System.out.println("ADD");
            }else if(x instanceof DivOperation){
                System.out.println("Div");
            }
        }

    }
}
