package models;

public class DivisionByZero extends Exception {
    public DivisionByZero() {
        super("Division by zero");
    }
}
