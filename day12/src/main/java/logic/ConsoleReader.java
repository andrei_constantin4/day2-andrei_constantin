package logic;

import com.tora.App;
import models.DivisionByZeroException;
import models.Statistics;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.EmptyStackException;

public class ConsoleReader {
    private static final Parser parser = new Parser();
    private static final BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
    private static final Statistics stats = new Statistics();
    private static final Logger LOGGER = LoggerFactory.getLogger(App.class);

    public void loop() throws IOException {
        System.out.println("Expression:");
        String s;
        while (!(s=reader.readLine()).equals("exit")) {
            computeInput(s);
            System.out.println("Expression:");
        }
        System.out.println("Exiting...");
    }

    private static void computeInput(String s) {
        switch (s) {
            case "history" -> printHistory();
            case "stats" -> printStats();
            default -> printResult(s);
        }
    }

    private static void printResult(String s) {
        LOGGER.debug("printResult method called");
        try {
            Double res = parser.evaluate(s, stats);
            System.out.println("Res: " + res);
        } catch (DivisionByZeroException e) {
            System.out.println("Division By Zero Exception");
        } catch (NullPointerException | EmptyStackException | IllegalStateException |
                 IllegalArgumentException e) {
            System.out.println("Invalid Input");
        }
    }

    private static void printStats() {
        LOGGER.debug("printStats method called");
        stats.getMap().forEach((key, value) -> System.out.println("Operation = " + key + ", Value = " + value));
    }

    private static void printHistory() {
        LOGGER.debug("printHistory method called");
        stats.getHistory().forEach(System.out::println);
    }
}
