package logic;

import com.tora.App;
import models.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Stack;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Parser {
    /*public Operation<Double> parse(String s, Statistics stats, Double lastRes) {
        String[] splitted = s.split("\\s+");
        Operation<Double> op;
        if (splitted.length > 3) {
            return null;
        }
        switch (splitted[0]) {
            case "+":
                op = new AddOperation(Double.valueOf(splitted[1]), Double.valueOf(splitted[2]));
                stats.add(OperationsEnum.ADD_OPERATION, op);
                return op;
            case "-":
                op = new SubOperation(Double.valueOf(splitted[1]), Double.valueOf(splitted[2]));
                stats.add(OperationsEnum.SUB_OPERATION, op);
                return op;
            case "*":
                op = new MultOperation(Double.valueOf(splitted[1]), Double.valueOf(splitted[2]));
                stats.add(OperationsEnum.MULT_OPERATION, op);
                return op;
            case "/":
                op = new DivOperation(Double.valueOf(splitted[1]), Double.valueOf(splitted[2]));
                stats.add(OperationsEnum.DIV_OPERATION, op);
                return op;
            case "max":
                op = new MaxOperation(Double.valueOf(splitted[1]), Double.valueOf(splitted[2]));
                stats.add(OperationsEnum.MAX_OPERATION, op);
                return op;
            case "min":
                op = new MinOperation(Double.valueOf(splitted[1]), Double.valueOf(splitted[2]));
                stats.add(OperationsEnum.MIN_OPERATION, op);
                return op;
            case "powerOf":
                op = new PowerOfOperation(Double.valueOf(splitted[1]), Double.valueOf(splitted[2]));
                stats.add(OperationsEnum.POWEROF_OPERATION, op);
                return op;
            case "sqrt":
                op = new SqrtOperation(Double.valueOf(splitted[1]));
                if (splitted.length == 3)
                    return null;
                stats.add(OperationsEnum.SQRT_OPERATION, op);
                return op;
            case "round":
                op = new RoundOperation(Double.valueOf(splitted[1]), Double.valueOf(splitted[2]));
                stats.add(OperationsEnum.ROUND_OPERATION, op);
                return op;
            default:
                throw new IllegalStateException("Unexpected value");
        }
    }*/

    /*public Double evaluate(String expression, Statistics stats) throws DivisionByZeroException,NullPointerException  {
        List<String> tokens = tokenize(expression);

        Stack<Double> values = new Stack<>();

        Stack<String> ops = new Stack<>();

        for (String token : tokens) {
            switch (token) {
                case ",":
                    while (!"min(".equals(ops.peek()) && !"max(".equals(ops.peek()) && !"round(".equals(ops.peek()) && !"powerOf(".equals(ops.peek())) {
                        String currentOp = ops.pop();
                        Operation<Double> op;
                        op = applyOp(currentOp, values.pop(), values.pop(), stats);
                        assert op != null;
                        values.push(op.compute());
                    }
                    break;
                case "(":
                case "min(":
                case "max(":
                case "round(":
                case "sqrt(":
                    ops.push(token);
                    break;
                case ")":
                    while (!"min(".equals(ops.peek()) && !"max(".equals(ops.peek()) && !"round(".equals(ops.peek()) && !"powerOf(".equals(ops.peek()) && !"(".equals(ops.peek()) && !"sqrt(".equals(ops.peek())) {
                        String currentOp = ops.pop();
                        Operation<Double> op;
                        op = applyOp(currentOp, values.pop(), values.pop(), stats);
                        assert op != null;
                        values.push(op.compute());
                    }
                    String lastOp = ops.pop();
                    if (!"(".equals(lastOp)) {
                        if (!"sqrt(".equals(lastOp)) {
                            Operation<Double> op;
                            op = applyOp(lastOp, values.pop(), values.pop(), stats);
                            assert op != null;
                            values.push(op.compute());
                        } else {
                            Operation<Double> op;
                            op = applyOp(lastOp, 0.0, values.pop(), stats);
                            assert op != null;
                            values.push(op.compute());
                        }
                    }
                    break;
                case "+":
                case "-":
                case "*":
                case "/":
                    while (!ops.empty() && hasPrecedence(token, ops.peek())) {
                        String currentOp = ops.pop();
                        Operation<Double> op;
                        op = applyOp(currentOp, values.pop(), values.pop(), stats);

                        assert op != null;
                        values.push(op.compute());
                    }
                    ops.push(token);
                    break;
                case "powerOf(":
                    break;
                default:
                    values.push(Double.parseDouble(token));
                    break;
            }
        }

        while (!ops.empty()) {
            String currentOp = ops.pop();
            Operation<Double> op;
            op = applyOp(currentOp, values.pop(), values.pop(), stats);
            assert op != null;
            values.push(op.compute());
        }

        return values.pop();
    }*/
    public Double evaluate(String expression, Statistics stats) throws DivisionByZeroException, NullPointerException {
        List<String> tokens = tokenize(expression);

        Stack<Double> values = new Stack<>();

        Stack<String> ops = new Stack<>();
        tokens.forEach((token) -> {
            switch (token) {
                case "," -> evaluateLeftFunction(stats, ops, values);
                case "(", "min(", "max(", "round(", "sqrt(", "powerOf(" -> ops.push(token);
                case ")" -> evaluateRightParentheses(stats, ops, values);
                case "+", "-", "*", "/" -> evaluateBasicOperation(stats, token, ops, values);
                default -> values.push(Double.parseDouble(token));
            }
        });

        evaluatedRemaining(stats, ops, values);

        return values.pop();
    }

    private static void evaluatedRemaining(Statistics stats, Stack<String> ops, Stack<Double> values) {
        while (!ops.empty()) {
            computeOperation(stats, ops, values);
        }
    }

    private static void computeOperation(Statistics stats, Stack<String> ops, Stack<Double> values) {
        String currentOp = ops.pop();
        Action<Double> op;
        op = applyOp(currentOp, values.pop(), values.pop(), stats);
        values.push(op.compute());
    }

    private static void evaluateBasicOperation(Statistics stats, String token, Stack<String> ops, Stack<Double> values) {
        while (!ops.empty() && hasPrecedence(token, ops.peek())) {
            computeOperation(stats, ops, values);
        }
        ops.push(token);
    }

    private static void evaluateRightParentheses(Statistics stats, Stack<String> ops, Stack<Double> values) {
        while (!"min(".equals(ops.peek()) && !"max(".equals(ops.peek()) && !"round(".equals(ops.peek()) && !"powerOf(".equals(ops.peek()) && !"(".equals(ops.peek()) && !"sqrt(".equals(ops.peek())) {
            computeOperation(stats, ops, values);
        }
        String lastOp = ops.pop();
        if (!"(".equals(lastOp)) {
            if (!"sqrt(".equals(lastOp)) {
                Action<Double> op;
                op = applyOp(lastOp, values.pop(), values.pop(), stats);
                values.push(op.compute());
            } else {
                Action<Double> op;
                op = applyOp(lastOp, 0.0, values.pop(), stats);
                values.push(op.compute());
            }
        }
    }

    private static void evaluateLeftFunction(Statistics stats, Stack<String> ops, Stack<Double> values) {
        while (!"min(".equals(ops.peek()) && !"max(".equals(ops.peek()) && !"round(".equals(ops.peek()) && !"powerOf(".equals(ops.peek())) {
            computeOperation(stats, ops, values);
        }
    }

    private static boolean hasPrecedence(String op1, String op2) {
        if ("(".equals(op2) || ")".equals(op2) || "max(".equals(op2) || "min(".equals(op2) || "round(".equals(op2) || "sqrt(".equals(op2) || "powerOf(".equals(op2))
            return false;
        return (!"*".equals(op1) && !"/".equals(op1)) ||
                (!"+".equals(op2) && !"-".equals(op2));
    }

    /*private static Operation<Double> applyOp(String op, Double b, Double a, Statistics stats) {
        Operation<Double> operation;
        switch (op) {
            case "+" -> {
                operation = new AddOperation(a, b);
                stats.add(OperationsEnum.ADD_OPERATION, operation);
                return operation;
            }
            case "-" -> {
                operation = new SubOperation(a, b);
                stats.add(OperationsEnum.SUB_OPERATION, operation);
                return operation;
            }
            case "*" -> {
                operation = new MultOperation(a, b);
                stats.add(OperationsEnum.MULT_OPERATION, operation);
                return operation;
            }
            case "/" -> {
                if (b == 0)
                    throw new DivisionByZeroException();
                operation = new DivOperation(a, b);
                stats.add(OperationsEnum.DIV_OPERATION, operation);
                return operation;
            }
            case "max(" -> {
                operation = new MaxOperation(a, b);
                stats.add(OperationsEnum.MAX_OPERATION, operation);
                return operation;
            }
            case "min(" -> {
                operation = new MinOperation(a, b);
                stats.add(OperationsEnum.MIN_OPERATION, operation);
                return operation;
            }
            case "round(" -> {
                operation = new RoundOperation(a, b);
                stats.add(OperationsEnum.ROUND_OPERATION, operation);
                return operation;
            }
            case "sqrt(" -> {
                operation = new SqrtOperation(a);
                stats.add(OperationsEnum.SQRT_OPERATION, operation);
                return operation;
            }
            case "powerOf(" -> {
                operation = new PowerOfOperation(a, b);
                stats.add(OperationsEnum.POWEROF_OPERATION, operation);
                return operation;
            }
            default -> {
            }
        }
        return null;
    }*/

    private static Action<Double> applyOp(String op, Double b, Double a, Statistics stats) {
        Action<Double> operation = null;
        switch (op) {
            case "+" -> operation = new AddOperation(a, b);
            case "-" -> operation = new SubOperation(a, b);
            case "*" -> operation = new MultOperation(a, b);
            case "/" -> operation = new DivOperation(a, b);
            case "max(" -> operation = new MaxOperation(a, b);
            case "min(" -> operation = new MinOperation(a, b);
            case "round(" -> operation = new RoundOperation(a, b);
            case "sqrt(" -> operation = new SqrtOperation(a);
            case "powerOf(" -> operation = new PowerOfOperation(a, b);
            default -> {
            }
        }
        stats.add(operation);
        return operation;
    }

    private List<String> tokenize(String s) {
        List<String> list = new ArrayList<>();
        Pattern pattern = Pattern.compile("round\\(|powerOf\\(|sqrt\\(|max\\(|\\(|\\)|min\\(|\\+|\\-|\\*|/|\\d+\\.?\\d*|,");
        Matcher matcher = pattern.matcher(s);
        String prevToken = "";
        String currentToken = "";
        int index = 0;
        while (matcher.find()) {
            currentToken = matcher.group(0);
            if (index == 0) {
                list.add("0");
                if (!currentToken.equals("+") && !currentToken.equals("-")) {
                    list.add("+");
                }
                list.add(currentToken);
            } else {
                if ((prevToken.equals("max(") || prevToken.equals("min(") || prevToken.equals("sqrt(") || prevToken.equals("powerOf(") || prevToken.equals("(") || prevToken.equals("round(") || prevToken.equals(",")) && (currentToken.equals("-") || currentToken.equals("+"))) {
                    list.add("0");
                }
                list.add(currentToken);
            }
            prevToken = currentToken;
            index++;
        }

        return list;
    }
}
