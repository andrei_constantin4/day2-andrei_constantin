package models;

import java.util.Objects;

public class BinaryOperation<T> extends UnaryOperation<T> {
    private final T y;

    public BinaryOperation(T x, T y) {
        super(x);
        this.y = y;
    }

    public T getY() {
        return y;
    }

    @Override
    public String toString() {
        return "BinaryOperator{" +
                "x=" + this.getX() +
                ", y=" + y +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        BinaryOperation<?> that = (BinaryOperation<?>) o;
        return this.getX() == y && this.getRes() == that.getRes();
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.getX(), y,getRes());
    }
}
