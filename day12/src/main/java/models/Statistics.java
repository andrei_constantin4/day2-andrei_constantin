package models;

import java.util.ArrayList;
import java.util.EnumMap;
import java.util.List;

public class Statistics {
    private final EnumMap<OperationsEnum, Integer> map = new EnumMap<>(OperationsEnum.class);
    private final List<Action<Double>> history = new ArrayList<>();

    public void add(Action<Double> currentOp) {
        map.merge(
                ((Operation<?>)currentOp).getOperationsEnum(),
                1,
                Integer::sum
        );
        history.add(currentOp);
    }

    public EnumMap<OperationsEnum, Integer> getMap() {
        return map.clone();
    }

    public List<Action<Double>> getHistory() {
        return new ArrayList<>(history);
    }
}
