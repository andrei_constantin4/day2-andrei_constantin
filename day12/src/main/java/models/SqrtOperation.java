package models;

public class SqrtOperation extends UnaryOperation<Double> implements Action<Double>,Validate {
    public SqrtOperation(Double x) {
        super(x);
        operationsEnum = OperationsEnum.SQRT_OPERATION;
    }

    @Override
    public Double compute() {

        setRes(Math.sqrt(getX()));
        return getRes();
    }

    @Override
    public String toString() {
        return "SqrtOperation{}" + " " + super.toString() + " Res=" + getRes();
    }

    @Override
    public boolean validate() {
        if (getX() < 0) {
            throw new IllegalArgumentException();
        }
        return true;
    }
}
