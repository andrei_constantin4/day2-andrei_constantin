package models;

public class PowerOfOperation extends BinaryOperation<Double> implements Action<Double> {
    public PowerOfOperation(Double x, Double y) {
        super(x, y);
        operationsEnum=OperationsEnum.POWEROF_OPERATION;
    }

    @Override
    public Double compute(){
        setRes(Math.pow(getX(),getY()));
        return getRes();
    }
    @Override
    public String toString() {
        return "PowerOfOperation{}"+" "+super.toString()+" Res="+getRes();
    }
}
