package models;

public class DivOperation extends BinaryOperation<Double> implements Action<Double>,Validate {
    public DivOperation(Double x, Double y) {
        super(x, y);
        operationsEnum = OperationsEnum.DIV_OPERATION;
    }

    @Override
    public Double compute() {
        setRes(getX() / getY());
        return getRes();
    }

    @Override
    public String toString() {
        return "DivOperation{}" + " " + super.toString() + " Res=" + getRes();
    }

    @Override
    public boolean validate() {
        if(getY()<epsilon){
            throw new DivisionByZeroException();
        }
        return true;
    }
}
