package models;

public class MinOperation extends BinaryOperation<Double> implements Action<Double> {
    public MinOperation(Double x, Double y) {
        super(x, y);
        operationsEnum=OperationsEnum.MIN_OPERATION;
    }

    @Override
    public Double compute() {
        setRes(Math.min(getX(), getY()));
        return getRes();
    }
    @Override
    public String toString() {
        return "MinOperation{}"+" "+super.toString()+" Res="+getRes();
    }
}
