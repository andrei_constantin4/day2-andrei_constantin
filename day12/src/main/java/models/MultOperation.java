package models;

public class MultOperation extends BinaryOperation<Double> implements Action<Double> {
    public MultOperation(Double x, Double y) {
        super(x, y);
        operationsEnum=OperationsEnum.MULT_OPERATION;
    }

    @Override
    public Double compute() {
        setRes(getX()*getY());
        return getRes();
    }
    @Override
    public String toString() {
        return "MultOperation{}"+" "+super.toString()+" Res="+getRes();
    }
}
