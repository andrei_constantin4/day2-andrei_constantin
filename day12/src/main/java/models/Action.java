package models;

public interface Action<T extends Number>{
    T compute();
}
