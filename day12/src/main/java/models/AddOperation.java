package models;

public class AddOperation extends BinaryOperation<Double> implements Action<Double> {

    public AddOperation(Double x, Double y) {
        super(x, y);
        operationsEnum = OperationsEnum.ADD_OPERATION;
    }

    @Override
    public Double compute() {
        setRes(getX() + getY());
        return getRes();
    }

    @Override
    public String toString() {
        return "AddOperation{}" + " " + super.toString() + " Res=" + getRes();
    }
}
