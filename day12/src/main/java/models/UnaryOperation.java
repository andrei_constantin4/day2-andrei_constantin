package models;

public class UnaryOperation<T> extends Operation<T> {
    private final T x;

    public UnaryOperation(T x) {
        this.x = x;
    }

    public T getX() {
        return x;
    }

    @Override
    public String toString() {
        return "UnaryOperator{" +
                "x=" + x +
                '}';
    }
}
