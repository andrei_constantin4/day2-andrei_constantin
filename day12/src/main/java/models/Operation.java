package models;

public abstract class Operation<T> {
    private T res;
    protected OperationsEnum operationsEnum;
    protected static final Double epsilon=0.00000001;

    protected T getRes() {
        return res;
    }

    protected void setRes(T res) {
        this.res = res;
    }

    public OperationsEnum getOperationsEnum() {
        return operationsEnum;
    }
}
