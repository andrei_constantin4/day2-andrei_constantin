package com.tora;

import logic.ConsoleReader;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;

/**
 * Hello world!
 *
 */
public class App 
{
    static Logger LOGGER = LoggerFactory.getLogger(App.class);
    public static void main( String[] args ) throws IOException {
        LOGGER.info("Initializing App");
        ConsoleReader consoleManage =new ConsoleReader();
        consoleManage.loop();
        LOGGER.warn("Ending application");
    }
}
