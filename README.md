Maven samples
==================

## Sample multi-module maven project integration with GitLab and Codacy

[![Codacy Badge](https://app.codacy.com/project/badge/Grade/9bb5bd0796ba4fbd929074a25bb56d4c)](https://www.codacy.com/gl/tora-train/maven-samples/dashboard?utm_source=gitlab.com&amp;utm_medium=referral&amp;utm_content=tora-train/2020/maven-samples&amp;utm_campaign=Badge_Grade)
[![Codacy Badge](https://app.codacy.com/project/badge/Coverage/9bb5bd0796ba4fbd929074a25bb56d4c)](https://www.codacy.com/gl/tora-train/maven-samples/dashboard?utm_source=gitlab.com&utm_medium=referral&utm_content=tora-train/2020/maven-samples&utm_campaign=Badge_Coverage)


This repository is intended as an example of how a multi-module Maven project can be setup and integrated with:
* **[Codacy](https://www.codacy.com "Codacy")** - static analysis front-end that uses tools for a number of languages. When integrated with your GitHub repo, it analyses your master branch and any other you select in the settings and gives you a summary of possible issues with your code. Not only that; whenever a new pull request is opened, it checks whether it fixes any of those issue (good) or it adds new ones (bad). This allows you to try and set a trend towards cleanliness, or at least to avoid getting further from it.
* **[GitLab](https://gitlab.com/)**

#### Codacy integration

After logging in with your GitLab account and giving the needed permissions, go ahead and add a project from the available projects. Codacy will analyse and review the project and then provide a dashboard with all sort of statistics.
![Codacy Login](/images/codacy_login.png?raw=true "Codacy Login")
![Codacy Add Organization](/images/codacy_add_organization.png?raw=true "Codacy Add Organization")
![Codacy Cloning](/images/codacy_cloning.png?raw=true "Codacy Cloning one you added a project")

Besides static code analysis, Codacy supports code coverage reports as well which will be provided by GitLab-CI after building and running the tests. For that a *Project API* integration is needed so that Codacy accepts coverage reports sent by CI server.
For this add a new integration (Project > Settings > Integrations : Add Integration)
![Codacy Project API](/images/codacy_api.png?raw=true "Codacy Project API")

Add the generated Project API as a variable in GiLab's project CI/CD settings (GitLab (project) > Settings > CI/CD : Variables)
![GitLab variable](/images/gitlab_codacy_token.png?raw=true "Gitlab Codacy project token")
- check gitlab-ci.yml for codacy integration for coverage

Codacy provide status Badges that can be integrated with GitLab:
![Codacy Badge](/images/codacy_badge.png?raw=true "Codacy Badge")

[![Codacy Badge](https://app.codacy.com/project/badge/Grade/c35b9c4b219643208e75b7e6b7dd864c)](https://app.codacy.com/gl/andrei_constantin4/day2-andrei_constantin/dashboard?utm_source=gl&utm_medium=referral&utm_content=&utm_campaign=Badge_grade)

