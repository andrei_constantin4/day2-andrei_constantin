package com.tora;

import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Callable;
import java.util.stream.Collectors;

import static java.lang.Math.toIntExact;

public class FilesRead implements Callable<String> {
    private final long startLocation;
    private final long chunkSize;
    private final FileChannel fileChannel;
    private final int fileIndex;
    private final Map<String, Integer> map;

    public FilesRead(long startLocation, long chunkSize, FileChannel fileChannel, int fileIndex, Map<String, Integer> map) {
        this.startLocation = startLocation;
        this.chunkSize = chunkSize;
        this.fileChannel = fileChannel;
        this.fileIndex = fileIndex;
        this.map = map;
    }

    @Override
    public String call() throws Exception {
        ByteBuffer buff = ByteBuffer.allocate(toIntExact(chunkSize));
        try {
            System.out.println("Reading the channel: " + startLocation + ":" + chunkSize+":"+fileIndex);
            //System.out.println("Block and index: " + _blockIndex + ":" + sequenceNumber);

            //Read file chunk to RAM
            fileChannel.read(buff,startLocation);

            //chunk to String
            String string_chunk = new String(buff.array(), StandardCharsets.UTF_8);
            System.out.println("Done reading the channel: " + startLocation + ":" + chunkSize+":"+fileIndex);
            List<String> stringList=string_chunk.lines().filter(s->s.contains("order new")).collect(Collectors.toList());
            for(String s:stringList){
                map.put(s,1);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return new String(buff.array(), StandardCharsets.UTF_8);
    }
}
