package com.tora;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.channels.FileChannel;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.*;
import java.util.concurrent.atomic.AtomicLong;

import static java.lang.Math.toIntExact;

public class App {

    public static void main(String[] args) throws IOException, InterruptedException {
        FileInputStream fileInputStream = new FileInputStream("./day13/res/data");
        FileChannel channel = fileInputStream.getChannel();
        long remainingSize = channel.size(); //get the total number of bytes in the file
        int NR_THREADS = 10;
        int NR_FILES = 4;
        long chunkSize = remainingSize / NR_THREADS; //file_size/threads
        List<BlockingQueue<Integer>> list = new ArrayList<>();
        List<File> files = new ArrayList<>();
        for (int i = 0; i < NR_FILES; i++) {
            list.add(new ArrayBlockingQueue<>(10000));
            files.add(new File("./day13/res/data_split" + i));
            if (Files.exists(files.get(i).toPath())) {
                Files.delete(files.get(i).toPath());
            }
            Files.createFile(files.get(i).toPath());
        }

//        if (chunk_size > (Integer.MAX_VALUE - 5))
//        {
//            chunk_size = (Integer.MAX_VALUE - 5);
//        }
        chunkSize = 100000000;

        ExecutorService executor = Executors.newFixedThreadPool(NR_THREADS);
        AtomicLong[] atomicLong = {new AtomicLong(0)};
        long start_loc = 0;
        int i = 0;
        long currentFileAddr = remainingSize / NR_FILES;
        long fileIncrement = remainingSize / NR_FILES;
        int currentFileIndex = 0;
        while (remainingSize >= chunkSize) {
            list.get(currentFileIndex).put(i);

            executor.execute(new FileRead(start_loc, toIntExact(chunkSize), channel, i, currentFileIndex, list, files, atomicLong));
            remainingSize = remainingSize - chunkSize;
            start_loc = start_loc + chunkSize;
//            start_loc=start_loc+atomicLong[0].get();
            i++;
            if (start_loc >= currentFileAddr) {
                currentFileIndex++;
                currentFileAddr += fileIncrement;
            }
        }
        list.get(currentFileIndex).add(i);

        executor.execute(new FileRead(start_loc, toIntExact(remainingSize), channel, i, currentFileIndex, list, files, atomicLong));


        executor.shutdown();
        executor.awaitTermination(1000, TimeUnit.SECONDS);
//        //Wait for all threads to finish
//        while (!executor.isTerminated()) {
//            //wait for infinity time
//        }
        System.out.println("Finished all threads");
        fileInputStream.close();
    }
}
