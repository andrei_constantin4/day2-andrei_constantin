package com.tora;

import java.io.*;
import java.nio.channels.FileChannel;
import java.util.*;
import java.util.concurrent.*;

import static java.lang.Math.toIntExact;

public class App2 {
    public static List<Long> getChunks(RandomAccessFile file,long chunk) throws IOException {
        long newAddress;
        long remainingSize=file.length();
        long currentAddress=chunk;
        List<Long>myList=new ArrayList<>();

//        while(remainingSize>=){
//            file.seek(currentAddress);
//            String nextString=file.readLine();
//            long newChunk=chunk+nextString.length();
//            newAddress+=currentAddress+newChunk;
//            currentAddress=newAddress+chunk;
//            currentAddress-
//        }
        myList.add(remainingSize);
        //here
        return myList;
    }
    public static long getChannelsSize(List<FileChannel>list) throws IOException {
        long mySize=0;
        for(FileChannel fileChannel:list){
            mySize+=fileChannel.size();
        }
        return mySize;
    }

    public static void main(String[] args) throws IOException, InterruptedException {
        int NR_THREADS = 1;
//        String[] list={
//                "./day13/res/data_split0",
//                "./day13/res/data_split1",
//                "./day13/res/data_split2",
//                "./day13/res/data_split3"
//        };
//        ListOfFiles mylist = new ListOfFiles(list);
        List<FileChannel>myList=List.of(
                new FileInputStream("./day13/res/data_split0").getChannel(),
                new FileInputStream("./day13/res/data_split1").getChannel(),
                new FileInputStream("./day13/res/data_split2").getChannel(),
                new FileInputStream("./day13/res/data_split3").getChannel()
                );
        int currentFileIndex = 0;
        long remainingSize=getChannelsSize(myList);
        long chunkSize=100000000;
        long startLocation=0;

        ExecutorService executor = Executors.newFixedThreadPool(NR_THREADS);
        System.out.println(remainingSize);
        Map<String, Integer> map = new ConcurrentHashMap<>();

        while (remainingSize >= chunkSize){
            executor.submit(new FilesRead(startLocation,toIntExact(chunkSize),myList.get(currentFileIndex), currentFileIndex, map));
            remainingSize = remainingSize - chunkSize;
            startLocation = startLocation + chunkSize;
            if (startLocation >= myList.get(currentFileIndex).size()) {
                currentFileIndex++;
                startLocation=0;
            }
        }
        executor.submit(new FilesRead(startLocation,toIntExact(remainingSize),myList.get(currentFileIndex), currentFileIndex, map));;
        //Tear Down
        executor.shutdown();
//
//        //Wait for all threads to finish
        executor.awaitTermination(1000, TimeUnit.SECONDS);
        System.out.println("Finished all threads and found "+map.size()+" unique entries");
    }
}
