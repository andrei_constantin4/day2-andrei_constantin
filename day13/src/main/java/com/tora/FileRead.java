package com.tora;

import java.io.*;
import java.nio.*;
import java.nio.channels.*;
import java.util.List;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.locks.ReentrantLock;

public class FileRead implements Runnable {

    private final FileChannel channel;
    private final long startLocation;
    private final int size;
    int sequenceNumber;
    int blockIndex;
    List<BlockingQueue<Integer>>list;
    List<File>files;
    ReentrantLock lock = new ReentrantLock();
    AtomicLong[] atomicLong;

    public FileRead(long loc, int size, FileChannel chnl, int sequence, int blockIndex, List<BlockingQueue<Integer>> list, List<File>files, AtomicLong[] atomicLong) {
        startLocation = loc;
        this.size = size;
        channel = chnl;
        sequenceNumber = sequence;
        this.blockIndex =blockIndex;
        this.list=list;
        this.files=files;
        this.atomicLong=atomicLong;
    }

    @Override
    public void run() {
        try {
            System.out.println("Reading the channel: " + startLocation + ":" + size);
            System.out.println("Block and index: " + blockIndex + ":" + sequenceNumber);

            //allocate memory
            ByteBuffer buff = ByteBuffer.allocate(size);

            //Read file chunk to RAM
            channel.read(buff, startLocation);

            //chunk to String
//            lock.lock();
//            String string_chunk = new String(buff.array(), StandardCharsets.UTF_8);
//            int k=string_chunk.length()-1;
//            try{
//                while(string_chunk.charAt(k)!='\n'){
//                    k--;
//                }
//                atomicLong[0].set(k);
//            }finally{
//                lock.unlock();
//            }
            //System.out.println("Done Reading the channel: " + _startLocation + ":" + _size+" "+k);
            System.out.println("Waiting block and index: " + blockIndex + ":" + sequenceNumber);
            //TO CHANGE THIS
//            while(list.get(blockIndex).peek()!= sequenceNumber){
//
//            }
            synchronized (list.get(blockIndex)){
                try {
                    Integer peeked=list.get(blockIndex).peek();
                    while(peeked!= sequenceNumber){
                        list.get(blockIndex).wait();
                        peeked=list.get(blockIndex).peek();
                    }

                } catch (InterruptedException e) {
                    System.err.println(Thread.currentThread().getName()+" Interrupted");
                }
            }

            synchronized(list.get(blockIndex)) {
                list.get(blockIndex).take();
                list.get(blockIndex).notifyAll();
            }
            OutputStream os = new FileOutputStream(files.get(blockIndex), true);
            os.write(buff.array(), 0, buff.array().length);
            System.out.println("Done block and index: " + blockIndex + ":" + sequenceNumber);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}