package com.tora;

import java.nio.channels.FileChannel;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.*;
import java.io.*;

class ListOfFiles implements Enumeration {

    String[] listOfFiles;
    int current = 0;

    ListOfFiles(String[] listOfFiles) {
        this.listOfFiles = listOfFiles;
    }

    public boolean hasMoreElements() {
        if (current < listOfFiles.length)
            return true;
        else
            return false;
    }

    public Object nextElement() {
        InputStream is = null;

        if (!hasMoreElements())
            throw new NoSuchElementException("No more files.");
        else {
            try {
                String nextElement = listOfFiles[current];
                current++;
                is = new FileInputStream(nextElement);
            } catch (FileNotFoundException e) {
                System.out.println("ListOfFiles: " + e);
            }
        }
        return is;
    }
    public long getSize() throws IOException {
        long sz=0;
        for(String s:listOfFiles){
            sz+= Files.size(Path.of(s));
        }
        return sz;
    }
}

