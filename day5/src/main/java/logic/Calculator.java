package logic;

import models.DivisionByZero;
import models.Operation;

import java.util.List;

public class Calculator {
    public double resultOf(List<Operation>list) throws DivisionByZero {
        double res=0;
        for(Operation op:list){
            res=op.compute(res);
        }
        return res;
    }
}
