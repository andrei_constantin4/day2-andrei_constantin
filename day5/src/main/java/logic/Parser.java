package logic;

import models.*;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Parser {
        private static final String myPattern="([+-/*]?)(\\d+\\.?\\d*)";
    public List<Operation> parse(String s){
        List<Operation>list=new ArrayList<>();
        Pattern pattern = Pattern.compile(myPattern);
        Matcher matcher = pattern.matcher(s);
        while (matcher.find()) {
            switch (matcher.group(1)) {
                case "":
                case "+":
                    list.add(new AddOperation(Double.parseDouble(matcher.group(2))));
                    break;
                case "-":

                    list.add(new SubOperation(Double.parseDouble(matcher.group(2))));
                    break;
                case "/":

                    list.add(new DivOperation(Double.parseDouble(matcher.group(2))));
                    break;
                case "*":

                    list.add(new MultOperation(Double.parseDouble(matcher.group(2))));
                    break;
                default:
                    break;
            }
        }
        return list;
    }
}
