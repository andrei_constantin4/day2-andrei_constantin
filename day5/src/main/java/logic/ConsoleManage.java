package logic;

import models.DivisionByZero;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class ConsoleManage {

    private static final Calculator calculator=new Calculator();
    private static final Parser parser=new Parser();
    private static final BufferedReader reader=new BufferedReader(new InputStreamReader(System.in));


    public void loop() throws IOException {
        System.out.println("Expression:");
        String s = reader.readLine();
        while (!"exit".equals(s)) {
            try {
                double res = calculator.resultOf(parser.parse(s));
                System.out.println("Result is: " + res);
            } catch (DivisionByZero e) {
                System.err.println("Division by zero");
            }
            System.out.println("Expression:");
            s = reader.readLine();
        }
        System.out.println("Exiting...");
    }

}
