package com.tora;

import logic.ConsoleManage;

import java.io.IOException;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        ConsoleManage consoleManage =new ConsoleManage();
        try {
            consoleManage.loop();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
