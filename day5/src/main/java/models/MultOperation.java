package models;

public class MultOperation extends Operation{
    public MultOperation(double x) {
        super(x);
    }

    @Override
    public double compute(double y){
        return this.getX()*y;
    }
}
