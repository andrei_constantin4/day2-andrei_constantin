package models;

public class AddOperation extends Operation {

    public AddOperation(double x) {
        super(x);
    }

    @Override
    public double compute(double y) {
        return this.getX()+y;
    }
}
