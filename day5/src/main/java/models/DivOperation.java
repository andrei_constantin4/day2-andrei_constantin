package models;

import static java.lang.Math.abs;

public class DivOperation extends Operation{

    public DivOperation(double x) {
        super(x);
    }

    @Override
    public double compute(double y) throws DivisionByZero {
        if(abs(this.getX()-epsilon)<=epsilon){
            throw new DivisionByZero();
        }
        return this.getX()/y;
    }
}
