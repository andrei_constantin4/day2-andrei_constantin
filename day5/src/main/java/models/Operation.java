package models;

import java.util.Objects;

public abstract class Operation {
    private double x;
    protected final static double epsilon=0.00000001;
    public Operation(double x) {
        this.x = x;
    }

    public double getX() {
        return x;
    }

    public abstract double compute(double y) throws DivisionByZero;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Operation operation = (Operation) o;
        return Double.compare(operation.x, x) == 0;
    }

    @Override
    public int hashCode() {
        return Objects.hash(x);
    }
}
