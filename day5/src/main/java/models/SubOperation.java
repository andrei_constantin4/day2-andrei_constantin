package models;

public class SubOperation extends Operation{

    public SubOperation(double x) {
        super(x);
    }

    @Override
    public double compute(double y) {
        return this.getX()-y;
    }
}
