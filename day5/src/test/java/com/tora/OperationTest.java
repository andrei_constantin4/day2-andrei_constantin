package com.tora;

import models.*;
import org.junit.Test;

import static java.lang.Math.abs;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.number.OrderingComparison.greaterThan;
import static org.junit.Assert.assertThrows;
import static org.junit.Assert.assertTrue;

public class OperationTest {
    private final static double epsilon=0.00000001;
    @Test
    public void testForAddValues() {
        AddOperation addOperation=new AddOperation(2.2);
        double x=addOperation.compute(1.1);
        double expected=3.3;
        assertThat(epsilon, greaterThan(abs(x-expected)));
    }

    @Test
    public void testForDivisionByZero() {
        DivOperation divOperation=new DivOperation(0);
        Exception exception = assertThrows(DivisionByZero.class, () -> divOperation.compute(-10));
        String expectedMessage = "Division by zero";
        String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(expectedMessage));
    }

    @Test
    public void testForSubValues() {
        SubOperation subOperation=new SubOperation(2.2);
        double x=subOperation.compute(1.1);
        double expected=1.1;
        assertThat(epsilon, greaterThan(abs(x-expected)));
    }

    @Test
    public void testForMulValues() {
        MultOperation multOperation=new MultOperation(3.2);
        double x=multOperation.compute(2.0);
        double expected=6.4;
        assertThat(epsilon, greaterThan(abs(x-expected)));
    }

    @Test
    public void testForDivValues() throws DivisionByZero {
        DivOperation divOperation=new DivOperation(3.2);
        double x=divOperation.compute(2.0);
        double expected=1.6;
        assertThat(epsilon, greaterThan(abs(x-expected)));
    }
}
