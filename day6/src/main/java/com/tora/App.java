package com.tora;

import logic.ConsoleReader;
import models.AddOperation;

import java.io.IOException;
import java.util.function.BinaryOperator;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args ) throws IOException {
        ConsoleReader consoleManage =new ConsoleReader();
        consoleManage.loop();
    }
}
