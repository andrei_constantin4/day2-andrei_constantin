package logic;

import models.DivisionByZeroException;
import models.Statistics;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.EmptyStackException;

public class ConsoleReader {
    private static final Parser parser = new Parser();
    private static final BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
    private static final Statistics stats = new Statistics();

    public void loop() throws IOException {
        System.out.println("Expression:");
        String s = reader.readLine();
        while (!"exit".equals(s)) {
            switch (s) {
                case "history" -> stats.getHistory().forEach(System.out::println);
                case "stats" -> stats.getMap().forEach((key, value) -> System.out.println("Operation = " + key + ", Value = " + value));
                default -> {
                    try {
                        Double res = parser.evaluate(s, stats);
                        System.out.println("Res: " + res);
                    } catch (DivisionByZeroException e) {
                        System.err.println("Division By Zero Exception");
                    } catch (NullPointerException | EmptyStackException | IllegalStateException |
                             IllegalArgumentException e) {
                        System.err.println("Invalid Input");
                    }
                }
            }
            System.out.println("Expression:");
            s = reader.readLine();
        }
        System.out.println("Exiting...");
    }
}
