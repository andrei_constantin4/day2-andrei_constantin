package models;

/*public enum OperationsEnum {
    ADD_OPERATION, SUB_OPERATION, DIV_OPERATION, MULT_OPERATION, MIN_OPERATION, MAX_OPERATION, POWEROF_OPERATION, ROUND_OPERATION, SQRT_OPERATION
}*/

import java.util.function.DoubleBinaryOperator;

public enum OperationsEnum implements DoubleBinaryOperator {
    ADD_OPERATION("+"){
        @Override
        public double applyAsDouble(double left, double right) {
            return left+right;
        }
    }, SUB_OPERATION("-") {
        @Override
        public double applyAsDouble(double left, double right) {
            return 0;
        }
    }, DIV_OPERATION("/") {
        @Override
        public double applyAsDouble(double left, double right) {
            return 0;
        }
    }, MULT_OPERATION("*") {
        @Override
        public double applyAsDouble(double left, double right) {
            return 0;
        }
    }, MIN_OPERATION("min(") {
        @Override
        public double applyAsDouble(double left, double right) {
            return 0;
        }
    }, MAX_OPERATION("max(") {
        @Override
        public double applyAsDouble(double left, double right) {
            return 0;
        }
    }, POWEROF_OPERATION("powerOf") {
        @Override
        public double applyAsDouble(double left, double right) {
            return 0;
        }
    }, ROUND_OPERATION("round(") {
        @Override
        public double applyAsDouble(double left, double right) {
            return 0;
        }
    }, SQRT_OPERATION("sqrt(") {
        @Override
        public double applyAsDouble(double left, double right) {
            return 0;
        }
    };

    private final String symbol;

    OperationsEnum(String s) {
        symbol=s;
    }

    public String getSymbol() {
        return symbol;
    }
}
