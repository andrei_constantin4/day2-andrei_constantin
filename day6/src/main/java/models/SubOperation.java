package models;

public class SubOperation extends BinaryOperator<Double> implements Operation<Double>{
    public SubOperation(Double x, Double y) {
        super(x, y);
        operationsEnum=OperationsEnum.SUB_OPERATION;
    }

    @Override
    public Double compute() {
        setRes(getX()-getY());
        return getRes();
    }
    @Override
    public String toString() {
        return "SubOperation{}"+" "+super.toString()+" Res="+getRes();
    }
}
