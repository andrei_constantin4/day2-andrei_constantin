package models;

public class UnaryOperator<T> extends Operator<T>{
    private final T x;

    public UnaryOperator(T x) {
        this.x = x;
    }

    public T getX() {
        return x;
    }

    @Override
    public String toString() {
        return "UnaryOperator{" +
                "x=" + x +
                '}';
    }
}
