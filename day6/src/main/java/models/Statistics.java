package models;

import java.util.ArrayList;
import java.util.EnumMap;
import java.util.List;

public class Statistics {
    private final EnumMap<OperationsEnum, Integer> map = new EnumMap<>(OperationsEnum.class);
    private final List<Operation<Double>> history = new ArrayList<>();

    public void add(Operation<Double> currentOp) {
        map.merge(
                ((Operator<?>)currentOp).getOperationsEnum(),
                1,
                Integer::sum
        );
        history.add(currentOp);
    }

    public EnumMap<OperationsEnum, Integer> getMap() {
        return map.clone();
    }

    public List<Operation<Double>> getHistory() {
        return new ArrayList<>(history);
    }
}
