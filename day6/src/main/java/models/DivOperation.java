package models;

public class DivOperation extends BinaryOperator<Double> implements Operation<Double>,Validate {
    public DivOperation(Double x, Double y) {
        super(x, y);
        operationsEnum = OperationsEnum.DIV_OPERATION;
    }

    @Override
    public Double compute() {
        if (getY() < epsilon) {
            throw new DivisionByZeroException();
        }
        setRes(getX() / getY());
        return getRes();
    }

    @Override
    public String toString() {
        return "DivOperation{}" + " " + super.toString() + " Res=" + getRes();
    }

    @Override
    public boolean validate() {
        if(getY()<epsilon){
            throw new DivisionByZeroException();
        }
        return true;
    }
}
