package models;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class RoundOperation extends BinaryOperator<Double> implements Operation<Double>{

    public RoundOperation(Double x, Double y) {
        super(x, y);
        operationsEnum=OperationsEnum.ROUND_OPERATION;
    }

    @Override
    public Double compute(){
        if(getY()<0) throw new IllegalArgumentException();
        BigDecimal bigDecimal=BigDecimal.valueOf(getX());
        bigDecimal=bigDecimal.setScale(getY().intValue(), RoundingMode.HALF_UP);
        setRes(bigDecimal.doubleValue());
        return getRes();
    }
    @Override
    public String toString() {
        return "RoundOperation{}"+" "+super.toString()+" Res="+getRes();
    }
}
