package models;

public interface Operation<T extends Number>{
    Double epsilon=0.00000001;
    abstract T compute();
}
