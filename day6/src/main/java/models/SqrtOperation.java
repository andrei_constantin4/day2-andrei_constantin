package models;

public class SqrtOperation extends UnaryOperator<Double> implements Operation<Double> {
    public SqrtOperation(Double x) {
        super(x);
        operationsEnum = OperationsEnum.SQRT_OPERATION;
    }

    @Override
    public Double compute() {
        if (getX() < 0) {
            throw new IllegalArgumentException();
        }
        setRes(Math.sqrt(getX()));
        return getRes();
    }

    @Override
    public String toString() {
        return "SqrtOperation{}" + " " + super.toString() + " Res=" + getRes();
    }
}
