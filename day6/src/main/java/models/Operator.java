package models;

public abstract class Operator<T> {
    private T res;
    protected OperationsEnum operationsEnum;

    protected T getRes() {
        return res;
    }

    protected void setRes(T res) {
        this.res = res;
    }

    public OperationsEnum getOperationsEnum() {
        return operationsEnum;
    }
}
