package models;

public class MaxOperation extends BinaryOperator<Double> implements Operation<Double>{
    public MaxOperation(Double x, Double y) {
        super(x, y);
        operationsEnum=OperationsEnum.MAX_OPERATION;
    }

    @Override
    public Double compute() {
        setRes(Math.max(getX(), getY()));
        return getRes();
    }
    @Override
    public String toString() {
        return "MaxOperation{}"+" "+super.toString()+" Res="+getRes();
    }
}
