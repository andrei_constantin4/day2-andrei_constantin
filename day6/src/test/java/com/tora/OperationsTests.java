package com.tora;

import models.*;
import org.junit.Test;

import static java.lang.Math.abs;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.number.OrderingComparison.greaterThan;
import static org.junit.Assert.assertThrows;
import static org.junit.Assert.assertTrue;

public class OperationsTests {
    private final static Double epsilon = 0.00000001;

    @Test
    public void testForAddValues() {
        AddOperation addOperation = new AddOperation(2.2, 2.1);
        Double x = addOperation.compute();
        double expected = 4.3;
        assertThat(epsilon, greaterThan(abs(x - expected)));
    }

    @Test
    public void testForDivisionByZero() {
        DivOperation divOperation = new DivOperation(1.0, 0.0);
        Exception exception = assertThrows(DivisionByZeroException.class, divOperation::compute);
        String expectedMessage = "Division by zero";
        String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(expectedMessage));
    }

    @Test
    public void testForSubValues() {
        SubOperation subOperation = new SubOperation(2.2, 1.0);
        double x = subOperation.compute();
        double expected = 1.2;
        assertThat(epsilon, greaterThan(abs(x - expected)));
    }

    @Test
    public void testForMulValues() {
        MultOperation multOperation = new MultOperation(3.2, 2.0);
        double x = multOperation.compute();
        double expected = 6.4;
        assertThat(epsilon, greaterThan(abs(x - expected)));
    }

    @Test
    public void testForDivValues() {
        DivOperation divOperation = new DivOperation(3.2, 2.0);
        double x = divOperation.compute();
        double expected = 1.6;
        assertThat(epsilon, greaterThan(abs(x - expected)));
    }

    @Test
    public void testForMaxValues() {
        MaxOperation maxOperation = new MaxOperation(3.2, 2.0);
        Double x = maxOperation.compute();
        double expected = 3.2;
        assertThat(epsilon, greaterThan(abs(x - expected)));
    }

    @Test
    public void testForMinValues() {
        MinOperation minOperation = new MinOperation(3.2, 2.0);
        Double x = minOperation.compute();
        Double expected = 2.0;
        assertThat(epsilon, greaterThan(abs(x - expected)));
    }

    @Test
    public void testForPowerOfValues() {
        PowerOfOperation powerOfOperation = new PowerOfOperation(2.0, 2.0);
        Double x = powerOfOperation.compute();
        Double expected = 4.0;
        assertThat(epsilon, greaterThan(abs(x - expected)));
    }

    @Test
    public void testForSqrtValues() {
        SqrtOperation sqrtOperation = new SqrtOperation(4.0);
        Double x = sqrtOperation.compute();
        Double expected = 2.0;
        assertThat(epsilon, greaterThan(abs(x - expected)));
    }

    @Test
    public void testForRoundValues() {
        RoundOperation roundOperation = new RoundOperation(2.22, 1.0);
        Double x = roundOperation.compute();
        Double expected = 2.2;
        assertThat(epsilon, greaterThan(abs(x - expected)));
    }
}
