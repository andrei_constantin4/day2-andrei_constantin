package com.tora;

import logic.Parser;
import models.*;
import org.junit.Test;

import static java.lang.Math.abs;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.number.OrderingComparison.greaterThan;
import static org.junit.Assert.assertThrows;
import static org.junit.Assert.assertTrue;

public class ComputeTests {
    private final static Double epsilon = 0.00000001;
    @Test
    public void test1() {
        Parser parser=new Parser();
        Statistics statistics=new Statistics();
        Double x=parser.evaluate("(2+3)*2",statistics);
        Double expected=10.0;
        assertThat(epsilon, greaterThan(abs(x - expected)));
    }

    @Test
    public void test2() {
        Parser parser=new Parser();
        Statistics statistics=new Statistics();
        Double x=parser.evaluate("sqrt(4)",statistics);
        Double expected=2.0;
        assertThat(epsilon, greaterThan(abs(x - expected)));
    }

    @Test
    public void test3() {
        Parser parser=new Parser();
        Statistics statistics=new Statistics();
        Double x=parser.evaluate("round(3.222,1)",statistics);
        Double expected=3.2;
        assertThat(epsilon, greaterThan(abs(x - expected)));
    }
    @Test
    public void test4() {
        Parser parser=new Parser();
        Statistics statistics=new Statistics();
        Double x=parser.evaluate("(2+max(1,sqrt(4)))",statistics);
        Double expected=4.0;
        assertThat(epsilon, greaterThan(abs(x - expected)));
    }
}
