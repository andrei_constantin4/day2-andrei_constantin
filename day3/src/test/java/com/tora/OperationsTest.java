package com.tora;

import org.junit.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.Assert.assertThrows;
import static org.junit.Assert.assertTrue;

public class OperationsTest {
    @Test
    public void testForPositiveValues() {
        Operations op = new Operations();
        int rez;
        try {
            rez = op.add(10, 2);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
        assertThat(rez, is(12));
    }

    @Test
    public void testForNegativeValues() {
        Operations op = new Operations();
        Exception exception = assertThrows(RuntimeException.class, () -> op.add(-10, -2));
        String expectedMessage = "Negative numbers input";
        String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(expectedMessage));
    }

    @Test
    public void testForMaxValues() {
        Operations op = new Operations();
        Exception exception = assertThrows(RuntimeException.class, () -> op.add(Integer.MAX_VALUE / 2, Integer.MAX_VALUE / 2));
        String expectedMessage = "Max value";
        String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(expectedMessage));
    }
}
