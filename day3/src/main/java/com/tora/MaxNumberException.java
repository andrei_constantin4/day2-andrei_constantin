package com.tora;

public class MaxNumberException extends RuntimeException {
    public MaxNumberException(String message) {
        super(message);
    }
}
