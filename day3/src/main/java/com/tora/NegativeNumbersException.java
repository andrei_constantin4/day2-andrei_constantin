package com.tora;

public class NegativeNumbersException extends RuntimeException {
    public NegativeNumbersException(String message) {
        super(message);
    }
}
