package com.tora;

public class Operations {
    public int add(int x, int y) {
        if (x < 0 || y < 0)
            throw new NegativeNumbersException("Negative numbers input");
        if (x == Integer.MAX_VALUE/2 && y == Integer.MAX_VALUE/2)
            throw new MaxNumberException("Max value");
        return x + y;
    }
}
